# Desafio Frontend Beblue #

### Construa uma aplicação para gerenciamento de favoritos (bookmark) conforme os itens abaixo: ###

* Três campos para cadastro do favorito, um para o título, outro para endereço e o último (um campo de texto) para cadastro de tags, as tags devem ser separadas por espaço.
* Um campo para filtrar os favoritos por tags, deve-se filtrar em tempo real, ou seja ao digitar.
* Uma lista com todos os favoritos exibidos, deve-se exibir título, endereço e tags, deve ser possível excluir cada tag individualmente dentro de cada item, deve ser possível excluir o favorito também individualmente.

### Requisitos: ###

* Utilizar React e Redux.
* Utilizar Webpack 2/3 para todo o kit do bundle.
* Criar um script npm para realizar o build de produção do projeto.
* Utilizar ES6.
* Hospede o código fonte em um repositório privado no bitbucket ou github.
* Fique a vontade com o layout da aplicação.

### Bonus: ###

* Criar um npm script que use docker para rodar o projeto em ambiente de desenvolvimento.
* Implementar testes unitários.

### Referências: ###

* [React](https://egghead.io/courses/start-using-react-to-build-web-applications)
* [Redux](https://egghead.io/courses/getting-started-with-redux)
* [Idiomatic Redux](https://egghead.io/courses/building-react-applications-with-idiomatic-redux)
* [Webpack](https://webpack.js.org/concepts/)