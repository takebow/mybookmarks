import React, { Component } from 'react';
import logo from '../images/logo.svg';
import iconTrash from '../images/icon-trash.svg';
import '../App.css';
import { connect } from 'react-redux';
import { addBookmark, deleteBookmark, deleteTag, filterBookmarks } from '../actions';

class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      title: '',
      url: '',
      tags: ''
    }
  }

  resetForm(event) {
    event.preventDefault();
    this.setState({title:'', url:'', tags: ''})
  }

  addBookmark() {
    this.props.addBookmark(this.state.title, this.state.url, this.state.tags.split(' '));
  }

  filterBookmarks(filter) {
    this.props.filterBookmarks(filter);
  }

  deleteBookmark(id) {
    this.props.deleteBookmark(id);
  }

  deleteTag(id, tag) {
    this.props.deleteTag(id, tag);
  }

  renderBookmarks() {
    const { bookmarks } = this.props;
    return (
      <div className="List-bookmarks">
        {
          bookmarks.map(bookmark => {
            return(
              <div className="Bookmark" key={bookmark.id}>
                <div className="Bookmark-title">{bookmark.title}</div>
                <div className="Bookmark-url">{bookmark.url}</div>
                <div className="Bookmark-tags">
                  {
                    bookmark.tags.map((tag, index) => {
                      return(
                        <span className="Tag-item" key={index}>
                          <span className="Tag-text">{tag}</span>
                          <a className="Tag-remove" onClick={() => this.deleteTag(bookmark.id, {tag})}>&times;</a>
                        </span>
                      )
                    })
                  }
                </div>
                <div className="Bookmark-actions">
                  <a className="Bookmark-delete" onClick={() => this.deleteBookmark(bookmark.id)}><img src={iconTrash} className="icon" alt="delete"/></a>
                </div>
              </div>
            )
          })
        }
      </div>
    )
  }

  render() {
    return (
      <div className="App">

        <header>
          <h1 className="App-title"><img src={logo} alt="logo"/></h1>
          <input className="Search" type="text" placeholder="search"
            onChange={e => this.filterBookmarks(e.target.value)}
          />
        </header>

        <section>
          <form action="" onSubmit={(event) => this.resetForm(event)}>
            <div className="Bookmark">
              <div className="Bookmark-title">
                <input
                  className="Field"
                  type="text"
                  name="newTitle"
                  placeholder="Title"
                  onChange={event => this.setState({title: event.target.value})}
                  value={this.state.title}
                />
              </div>
              <div className="Bookmark-url">
                <input
                  className="Field"
                  type="text"
                  name="newUrl"
                  placeholder="Url"
                  onChange={event => this.setState({url: event.target.value})}
                  value={this.state.url}
                />
              </div>
              <div className="Bookmark-tags">
                <span className="Tag-item">

                </span>
                <input
                  className="Field"
                  type="text"
                  name="newTags"
                  placeholder="Tags"
                  onChange={event => this.setState({tags: event.target.value})}
                  value={this.state.tags}
                />
              </div>
              <div className="Bookmark-actions">
                <button
                  className="Button-new"
                  onClick={() => this.addBookmark()}
                  >add new</button>
              </div>
            </div>
          </form>

          { this.renderBookmarks() }
        </section>

      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    bookmarks: state
  }
}

export default connect (mapStateToProps, { addBookmark, deleteBookmark, deleteTag, filterBookmarks })(App);
