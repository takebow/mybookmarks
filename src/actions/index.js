import { ADD_BOOKMARK, DELETE_BOOKMARK, DELETE_TAG, FILTER_BOOKMARKS } from '../constants';

export const addBookmark = (title, url, tags=[]) => {
  const action = {
    type: ADD_BOOKMARK,
    title,
    url,
    tags
  }
  console.log('action in addBookmark', action);
  return action;
}

export const deleteBookmark = (id) => {
  const action = {
    type: DELETE_BOOKMARK,
    id
  }
  console.log('deleting bookmark in actions', action);
  return action;
}

export const deleteTag = (id, tag) => {
  const action = {
    type: DELETE_TAG,
    id,
    tag
  }
  console.log('deleting tag in actions', action);
  return action;
}

export const filterBookmarks = (filter) => {
  const action = {
    type: FILTER_BOOKMARKS,
    filter
  }
  console.log('filtering bookmarks in actions', action);
  return action;
}
