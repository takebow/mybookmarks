import { ADD_BOOKMARK, DELETE_BOOKMARK, DELETE_TAG, FILTER_BOOKMARKS } from '../constants';

//add
const bookmark = (action) => {
  let { title, url, tags = [] } = action;
  return {
    id: Math.random(),
    title,
    url,
    tags
  }
}

//filterBookmark
const filterBookmarks = (state = [], filter) => {
  const bookmarks = state.filter(bookmark => bookmark.tags.toString().indexOf(filter) >= 0);
  return bookmarks;
}


//deleteBookmark
const deleteBookmark = (state = [], id) => {
  const bookmarks = state.filter(bookmark => bookmark.id !== id);
  return bookmarks;
}

//deleteTag
const deleteTag = (state, id, tag) => {
  return bookmarks;
}

const initialState = [
    {id: 1, title: 'Beblue - Seu dinheiro de volta.', url: 'https://www.beblue.com.br', tags: ['beblue', 'dinheiro', 'aplicativo']},
    {id: 2, title: 'Apple App Store - Beblue App', url: 'https://itunes.apple.com/br/app/beblue-app/id1033201079?l=en&mt=8', tags: ['beblue', 'apple', 'ios', 'aplicativo', 'download']},
    {id: 3, title: 'Android Play Store - beblue app', url: 'https://play.google.com/store/apps/details?id=br.com.beblue&hl=pt_BR', tags: ['beblue', 'android', 'play', 'store', 'aplicativo', 'download']},
    {id: 4, title: 'Beblue - credenciamento', url: 'https://www.beblue.com.br/credenciamento', tags: ['beblue', 'cadastro', 'lojista', 'credenciamento']},
    {id: 5, title: 'Beblue - Blog.', url: 'http://blog.beblue.com.br/', tags: ['beblue', 'blog']},
    {id: 6, title: 'Start Using React to Build Web Applications', url: 'https://egghead.io/courses/start-using-react-to-build-web-applications', tags: ['react', 'free', 'course']}
  ]

//reducers
const bookmarks = (state = initialState, action) => {

  let bookmarks, filtered;

  switch(action.type) {

    case ADD_BOOKMARK:
      bookmarks = [...state, bookmark(action)];
      // console.log('bookmarks as state', bookmarks);
      return bookmarks;

    case DELETE_BOOKMARK:
      bookmarks = deleteBookmark(state, action.id);
      return bookmarks;

    case DELETE_TAG:
      bookmarks = deleteTag(state, action.id, action.tag);
      return bookmarks;

    case FILTER_BOOKMARKS:
      bookmarks = initialState;
      if (action.filter) {
        filtered = filterBookmarks(state, action.filter);
      } else {
        filtered = bookmarks;
      }
      return filtered;

    default:
      return state;
  }
}

export default bookmarks;
